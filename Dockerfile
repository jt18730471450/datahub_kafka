FROM ubuntu:14.04

ENV TIME_ZONE=Asia/Shanghai
RUN ln -snf /usr/share/zoneinfo/$TIME_ZONE /etc/localtime && echo $TIME_ZONE > /etc/timezone

RUN apt-get update && apt-get upgrade -q -y
RUN apt-get install -q -y \
    scala

RUN mkdir /kafka
COPY ./kafka /kafka
WORKDIR /kafka

EXPOSE 2181 9092
ENTRYPOINT ["/kafka/start.sh"]
